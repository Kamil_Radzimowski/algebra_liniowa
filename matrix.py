import sys
import csv


# A = [(5., 3., 1.), [3., 9., 4.], [1., 3., 5.]]
# B = [[9.0], (16.0,), [9.0]]


def read_file():
    matrix = []
    with open('matrix.txt', newline='') as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            row_data = []
            for col in row:
                row_data.append(int(col))
            matrix.append(row_data)
    A = []
    B = []
    for i in range(len(matrix)):
        helper_A = []
        helper_B = []
        for j in range(len(matrix[i])):
            if j == len(matrix[i]) - 1:
                helper_B.append(matrix[i][j])
            else:
                helper_A.append(matrix[i][j])
        A.append(helper_A)
        B.append(helper_B)
    return A, B


def zeros_matrix(rows, cols):
    A = []
    for i in range(rows):
        A.append([])
        for j in range(cols):
            A[-1].append(0.0)

    return A


def copy_matrix(M):
    rows = len(M)
    cols = len(M[0])

    MC = zeros_matrix(rows, cols)

    for i in range(rows):
        for j in range(cols):
            MC[i][j] = M[i][j]

    return MC


def matrix_multiply(A, B):
    rowsA = len(A)
    colsA = len(A[0])

    rowsB = len(B)
    colsB = len(B[0])

    if colsA != rowsB:
        print('Number of A columns must equal number of B rows.')
        sys.exit()

    C = zeros_matrix(rowsA, colsB)

    for i in range(rowsA):
        for j in range(colsB):
            total = 0
            for ii in range(colsA):
                total += A[i][ii] * B[ii][j]
            C[i][j] = total

    return C


def do_postaci_schodkowej(A, B):
    AM = copy_matrix(A)
    n = len(A)
    BM = copy_matrix(B)

    indices = list(range(n))
    for fd in range(n):
        fdScaler = 1.0 / AM[fd][fd]
        for j in range(n):
            AM[fd][j] *= fdScaler
        BM[fd][0] *= fdScaler

        for i in indices[0:fd] + indices[fd + 1:]:
            crScaler = AM[i][fd]
            for j in range(n):
                AM[i][j] = AM[i][j] - crScaler * AM[fd][j]
            BM[i][0] = BM[i][0] - crScaler * BM[fd][0]

    return AM, BM


def results(AM, BM):
    rowsB = len(BM)
    variables = 0
    for i in range(len(AM[0])):
        is_all_zeroes = True
        for j in range(len(AM)):
            if AM[j][i] != 0:
                is_all_zeroes = False
        if not is_all_zeroes:
            variables += 1
    if rowsB < variables:
        print("Układ ma nieskończenie wiele rozwiązań")
    elif rowsB != variables:
        print("Układ jest sprzeczny")
    else:
        shorter = min(len(AM), len(AM[0]))
        for i in range(shorter):
            print(F"X{i + 1} = {BM[i][0]}")


A, B = read_file()
# print(A, B)
AM, BM = do_postaci_schodkowej(A, B)
print(AM, BM)
results(AM, BM)
