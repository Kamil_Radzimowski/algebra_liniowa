import csv


def read_file():
    matrix = []
    with open('matrix.txt', newline='') as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            row_data = []
            for col in row:
                row_data.append(int(col))
            matrix.append(row_data)
    return matrix


def swap_row(row_1, row_2, matrix):
    temp = matrix[row_1]
    matrix[row_1] = matrix[row_2]
    matrix[row_2] = temp


def argmax(i, k, matrix):
    max = matrix[i][k]
    max_index = abs(i)
    while i < len(matrix):
        if abs(matrix[i][k]) > max:
            max_index = i
            max = matrix[i][k]
        i += 1
    return max_index


# v Do postaci schodkowej v
def to_row_echelon_form(matrix):
    h = 0
    k = 0
    while h < len(matrix) and k < len(matrix[0]):
        i_max = argmax(h, k, matrix)
        if matrix[i_max][k] == 0:
            k += 1
        else:
            swap_row(h, i_max, matrix)
            for i in range(h + 1, len(matrix)):
                f = matrix[i][k] / matrix[h][k]
                matrix[i][k] = 0
                for j in range(k + 1, len(matrix[0])):
                    matrix[i][j] = matrix[i][j] - matrix[h][j] * f
            h += 1
            k += 1


def print_matrix(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            print(matrix[i][j], end="  ")
        print("")


def solve_matrix(matrix):
    result = []
    # sprawdz czy uklad jest sprzeczny
    all_are_zeroes = True
    for i in range(len(matrix[0]) - 1):
        if matrix[len(matrix) - 1][i] != 0:
            all_are_zeroes = False

    return result


data = read_file()
to_row_echelon_form(data)
print_matrix(data)
